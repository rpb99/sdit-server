const express = require("express");

const {
  register,
  login,
  currentUser,
  updateProfile,
  logout,
  getAll,
  destroy,
  getAllAvailable
} = require("../controllers/users");
const protect = require("../middlewares/auth");
const allow = require("../middlewares/permission");

const router = express.Router();

router.post("/register", register);
router.post("/login", login);
router.get("/current-user", protect, currentUser);
router.delete("/logout", logout);
router.put("/profile", protect, updateProfile);
router.get('/users', getAll)
router.get('/users/available', getAllAvailable)
router.delete("/users/:id_user", protect, allow('admin'), destroy);

module.exports = router;
