const express = require("express");

const {
  getAll,
} = require("../controllers/payment-logs");
const protect = require("../middlewares/auth");
const allow = require("../middlewares/permission");

const router = express.Router();

router.get("/logs", protect, allow("admin", "principal"), getAll);

module.exports = router;