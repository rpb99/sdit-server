const express = require("express");

const {
  create,
  getAll,
  getAllBills,
  getAllSuccessByStudent,
  get,
  webhook
} = require("../controllers/transaksi");
const protect = require("../middlewares/auth");
const allow = require("../middlewares/permission");

const router = express.Router();

router.get("/transactions", getAll);
router.get("/transactions/:transactionId", get);
router.get("/transactions/:studentId/success", getAllSuccessByStudent);
router.get("/transactions/:studentId/bills", getAllBills);
router.post("/transactions", protect, create);
router.post("/webhook", webhook);

module.exports = router;
