const express = require("express");

const {
  create,
  get,
  getAll,
  destroy,
  update,
} = require("../controllers/tahun-pelajaran");
const protect = require("../middlewares/auth");
const allow = require("../middlewares/permission");

const router = express.Router();

router.get("/tahun-pelajaran", getAll);
router.get("/tahun-pelajaran/:id_tp", get);
router.post("/tahun-pelajaran", protect, allow("admin"), create);
router.put("/tahun-pelajaran/:id_tp", protect, allow("admin"), update);
router.delete("/tahun-pelajaran/:id_tp", protect, allow("admin"), destroy);

module.exports = router;
