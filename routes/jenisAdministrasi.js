const express = require("express");

const {
  create,
  get,
  getAll,
  destroy,
  update,
} = require("../controllers/jenis-administrasi");
const protect = require("../middlewares/auth");
const allow = require("../middlewares/permission");

const router = express.Router();

router.get("/jenis-administrasi/:id_jenis_administrasi", get);
router.get("/jenis-administrasi", getAll);
router.post("/jenis-administrasi", protect, allow("admin"), create);
router.put("/jenis-administrasi/:id_jenis_administrasi", protect, allow("admin"), update);
router.delete("/jenis-administrasi/:id_jenis_administrasi", protect, allow("admin"), destroy);

module.exports = router;