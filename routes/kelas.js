const express = require("express");

const {
  create,
  get,
  getAll,
  destroy,
  update,
  getAllSelect
} = require("../controllers/kelas");
const protect = require("../middlewares/auth");
const allow = require("../middlewares/permission");

const router = express.Router();

router.get("/kelas", getAll);
router.get("/kelas/select", getAllSelect);
router.get("/kelas/:id_kelas", get);
router.post("/kelas", protect, allow("admin"), create);
router.put("/kelas/:id_kelas", protect, allow("admin"), update);
router.delete("/kelas/:id_kelas", protect, allow("admin"), destroy);

module.exports = router;
