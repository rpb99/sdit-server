const express = require("express");

const refreshToken = require("../controllers/refresh-token");
const protect = require("../middlewares/auth");

const router = express.Router();

router.get("/refresh-tokens", refreshToken);

module.exports = router;
