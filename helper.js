const { RefreshToken } = require("./models");
const { v4: uuidv4 } = require("uuid");

const sendTokenRes = async (user, statusCode, res) => {
  const refreshToken = user.generateRefreshToken();
  const accessToken = user.generateAccessToken();
  // const refreshTokenExist = await RefreshToken.findOne({
  //   where: { user_id: user.id },
  // });

  // if (refreshTokenExist)
  //   return res
  //     .status(403)
  //     .json({ status: "error", message: "you already logged in" });

  await RefreshToken.destroy({
    where: {
      user_id: user.id_user,
    },
  });

  await RefreshToken.create({
    id: uuidv4(),
    token: refreshToken,
    user_id: user.id_user,
  });

  const date = new Date()
  date.setDate(date.getDate() + 90);
  // date.setDate(date.getTime() + 10 * 1000);

  const options = {
    // sameSite: "strict",
    path: "/",
    expires: date,
    httpOnly: true,
  };

  if (process.env.NODE_ENV === "production") options.secure = true;

  return res.cookie("refreshToken", refreshToken, options)
    .json({
      success: true,
      accessToken,
      refreshToken,
    });
};

module.exports = { sendTokenRes };
