const randomStr = (length) => {
    let result = ''
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() *
            charactersLength));
    }
    return result;
}

const formatCapitalizedWord = (word) => {
    const wordsSplitted = word.split(' ')
    const result = wordsSplitted.reduce((res, item) => {
        const firstWord = item.charAt().toUpperCase()
        const otherWord = item.slice(1).toLowerCase()
        return [...res, firstWord + otherWord]
    }, [])

    return result.join(' ')
}

module.exports = { randomStr,formatCapitalizedWord }