const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
    const Transaksi = sequelize.define(
        "Transaksi",
        {
            id_transaksi: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            id_jenis_administrasi: {
                type: DataTypes.UUID,
                allowNull: false,
            },
            id_siswa: {
                type: DataTypes.UUID,
                allowNull: false,
            },
            snap_url: DataTypes.TEXT,
            metadata: DataTypes.JSON,
            status: {
                type: DataTypes.STRING(25),
                default: 'pending'
            },
            tgl_bayar: DataTypes.DATE,
            createdAt: {
                field: "created_at",
                type: DataTypes.DATE,
                allowNull: false,
            },
            updatedAt: {
                field: "updated_at",
                type: DataTypes.DATE,
                allowNull: false,
            },
        },
        {
            tableName: "transaksi",
            timestamps: true,
        }
    );

    Transaksi.beforeCreate((item) => (item.id_transaksi = uuidv4()));

    Transaksi.associate = (models) => {
        const { JenisAdministrasi, Student, PaymentLog } = models
        Transaksi.hasOne(PaymentLog, { foreignKey: "id_transaksi" })
        Transaksi.belongsTo(JenisAdministrasi, { foreignKey: "id_jenis_administrasi" });
        Transaksi.belongsTo(Student, { foreignKey: "id_siswa" });
    };

    return Transaksi;
};
