const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  const Kelas = sequelize.define(
    "Kelas",
    {
      id_kelas: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      nama_kelas: {
        type: DataTypes.STRING(30),
        allowNull: false,
      },
      id_tp: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      tableName: "kelas",
      timestamps: true,
    }
  );

  Kelas.beforeCreate((item) => (item.id_kelas = uuidv4()));

  Kelas.associate = (models) => {
    const {TahunPelajaran, Student} = models
    Kelas.belongsTo(TahunPelajaran, { foreignKey: "id_tp" });
    Kelas.hasOne(Student, {foreignKey: "id_kelas"})
  };

  return Kelas;
};
