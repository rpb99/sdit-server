const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  const PaymentLog = sequelize.define(
    "PaymentLog",
    {
      id_payment_logs: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      status: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      payment_type: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      raw_response: {
        type: DataTypes.JSON,
        allowNull: false,
      },
      id_transaksi: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      tableName: "payment_logs",
      timestamps: true,
    }
  );

  PaymentLog.beforeCreate((item) => (item.id_payment_logs = uuidv4()));

  PaymentLog.associate = (models) => {
    PaymentLog.belongsTo(models.Transaksi, { foreignKey: "id_transaksi" });
  };

  return PaymentLog;
};
