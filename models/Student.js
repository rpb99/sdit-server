const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  const Student = sequelize.define(
    "Student",
    {
      id_siswa: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      id_user: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      id_kelas: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      nis: {
        type: DataTypes.STRING(10),
        allowNull: false,
      },
      nama_siswa: {
        type: DataTypes.STRING(35),
        allowNull: false,
      },
      // telepon: {
      //   type: DataTypes.STRING(15),
      //   allowNull: false,
      // },
      // alamat: {
      //   type: DataTypes.TEXT,
      //   allowNull: false,
      // },
      // jenis_kelamin: {
      //   type: DataTypes.ENUM,
      //   values: ["P", "L"],
      //   allowNull: false,
      // },
      // telepon: {
      //   type: DataTypes.STRING,
      //   allowNull: false,
      // },
      status: {
        type: DataTypes.STRING,
        defaultValue: 'aktif'
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      tableName: "siswa",
      timestamps: true,
    }
  );

  Student.beforeCreate((item) => (item.id_siswa = uuidv4()));

  Student.associate = (models) => {
    const { Transaksi, User, Kelas } = models
    Student.belongsTo(User, { foreignKey: 'id_user' })
    Student.belongsTo(Kelas, { foreignKey: 'id_kelas' })
    Student.hasMany(Transaksi, { foreignKey: "id_siswa" })
  }

  return Student;
};
