const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  const TahunPelajaran = sequelize.define(
    "TahunPelajaran",
    {
      id_tp: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      nama_tp: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      tableName: "tahun_pelajaran",
      timestamps: true,
    }
  );

  TahunPelajaran.beforeCreate((item) => (item.id_tp = uuidv4()));

  TahunPelajaran.associate = (models) => {
    
    const { JenisAdministrasi, Kelas } = models

    TahunPelajaran.hasMany(JenisAdministrasi, { foreignKey: "id_jenis_administrasi" });
    TahunPelajaran.hasMany(Kelas, { foreignKey: "id_tp" });
  };

  return TahunPelajaran;
};
