const argon2 = require("argon2");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const {
  JWT_EXPIRE_IN,
  JWT_SECRET,
  JWT_REFRESH_TOKEN_SECRET,
  JWT_TOKEN_SECRET_EXPIRE_IN,
} = process.env;

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define(
    "User",
    {
      id_user: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      nama_user: {
        type: DataTypes.STRING(25),
        allowNull: false,
      },
      email: {
        type: DataTypes.STRING(35),
        allowNull: false,
        unique: true,
      },
      password: {
        type: DataTypes.STRING,
        // allowNull: false,
      },
      role: {
        type: DataTypes.ENUM,
        values: ["admin", "student", "principal"],
        allowNull: false,
        defaultValue: "student",
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      tableName: "users",
      timestamps: true,
    }
  );

  const encryptPassword = async (user) => {
    user.id_user = uuidv4();
    if (user.changed("password")) {
      user.password = await argon2.hash(user.password);
    } else {
      user.password = await argon2.hash('1234567890');
    }
  };

  User.beforeCreate(encryptPassword);
  User.beforeUpdate(encryptPassword);

  User.prototype.generateAccessToken = function () {
    return jwt.sign({ id_user: this.id_user }, JWT_SECRET, {
      expiresIn: '15s',
    });
  };

  User.prototype.generateRefreshToken = function () {
    // return jwt.sign({ id_user: this.id_user }, JWT_REFRESH_TOKEN_SECRET)
    return jwt.sign({ id_user: this.id_user }, JWT_REFRESH_TOKEN_SECRET, {
      expiresIn: JWT_TOKEN_SECRET_EXPIRE_IN,
    });
  };

  User.prototype.isMatch = async function (enteredPassword) {
    return await argon2.verify(this.password, enteredPassword);
  };

  User.associate = (models) => {
    const { Student } = models

    User.hasOne(Student, { foreignKey: 'id_user' })
  }

  return User;
};
