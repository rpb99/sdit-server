const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
    const Tagihan = sequelize.define(
        "Tagihan",
        {
            id_tagihan: {
                type: DataTypes.UUID,
                primaryKey: true,
            },
            id_jenis_spp: {
                type: DataTypes.UUID,
                allowNull: false,
                unique:true,
            },
            id_siswa: {
                type: DataTypes.UUID,
                allowNull: false,
            },
            nama_spp: {
                type: DataTypes.STRING,
                allowNull: false,
            },
            keterangan: {
                type: DataTypes.TEXT,
                allowNull: false,
            },
            nominal: {
                type: DataTypes.DOUBLE,
                allowNull: false,
            },
            createdAt: {
                field: "created_at",
                type: DataTypes.DATE,
                allowNull: false,
            },
            updatedAt: {
                field: "updated_at",
                type: DataTypes.DATE,
                allowNull: false,
            },
        },
        {
            tableName: "tagihan",
            timestamps: true,
        }
    );

    Tagihan.beforeCreate((item) => (item.id_tagihan = uuidv4()));

    Tagihan.associate = (models) => {
        const { JenisAdministrasi, Student,Transaksi } = models

        Tagihan.belongsTo(JenisAdministrasi, { foreignKey: "id_jenis_administrasi" });
        Tagihan.belongsTo(Student, { foreignKey: "id_siswa" });
    };

    return Tagihan;
};
