const { v4: uuidv4 } = require("uuid");

module.exports = (sequelize, DataTypes) => {
  const JenisAdministrasi = sequelize.define(
    "JenisAdministrasi",
    {
      id_jenis_administrasi: {
        type: DataTypes.UUID,
        primaryKey: true,
      },
      nama_administrasi: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      nominal: {
        type: DataTypes.DOUBLE(7, 0),
        allowNull: false,
      },
      keterangan: DataTypes.STRING(50),
      id_tp: {
        type: DataTypes.UUID,
        allowNull: false,
      },
      createdAt: {
        field: "created_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
      updatedAt: {
        field: "updated_at",
        type: DataTypes.DATE,
        allowNull: false,
      },
    },
    {
      tableName: "jenis_administrasi",
      timestamps: true,
    }
  );

  JenisAdministrasi.beforeCreate((item) => (item.id_jenis_administrasi = uuidv4()));

  JenisAdministrasi.associate = (models) => {
    const { Transaksi, TahunPelajaran } = models
    JenisAdministrasi.hasMany(Transaksi, { foreignKey: "id_jenis_administrasi" });
    JenisAdministrasi.belongsTo(TahunPelajaran, { foreignKey: "id_tp" });
  };

  return JenisAdministrasi;
};
