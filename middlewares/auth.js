const { User, RefreshToken } = require("../models");
const jwt = require("jsonwebtoken");

module.exports = async (req, res, next) => {
  let token;
  // const refreshToken = req.cookies.refreshToken ?? "";

  // const getRefreshToken = await RefreshToken.findOne({
  //   where: { token: refreshToken },
  // });
// console.log(JSON.stringify(req.cookies));
  // if (!getRefreshToken) {
  //   return res.status(401).json({
  //     success: false,
  //     message: "need a login",
  //   });
  // }

  // console.log("COOKIES :",req.cookies.refreshToken);

  if (req.headers.authorization) {
    token = req.headers.authorization.split(" ")[1];
  }

  // if (!token)
  //   return res.status(401).json({
  //     success: false,
  //     message: "Not authorized to access this route",
  //   });

  try {
    // verify token
    const decoded = jwt.verify(token, process.env.JWT_SECRET);
    req.user = await User.findByPk(decoded.id_user, {
      attributes: { exclude: ["password"] },
    });
    next();
  } catch (error) {
    console.log("My Error: ", error);
    return res.status(401).json({
      success: false,
      message: error?.message,
    });
  }
};
