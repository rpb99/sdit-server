const jwt = require("jsonwebtoken");
const { RefreshToken, User } = require("../../models");

const { JWT_REFRESH_TOKEN_SECRET, JWT_SECRET, JWT_EXPIRE_IN } = process.env;

module.exports = async (req, res) => {
  const refreshToken = req.cookies.refreshToken;

  if (!refreshToken) {
    return res.status(403).json({
      success: false,
      message: "need a login",
    });
  }

  const refreshTokenExist = await RefreshToken.findOne({
    where: { token: refreshToken },
  });

  if (!refreshTokenExist) return res.sendStatus(403);

  jwt.verify(refreshToken, JWT_REFRESH_TOKEN_SECRET, (err, decoded) => {
    if (err) return res.sendStatus(403);

    const accessToken = jwt.sign({ id_user: decoded.id_user }, JWT_SECRET, {
      expiresIn: JWT_EXPIRE_IN,
    });

    res.json({ accessToken });
  });
};
