const xlsx = require('xlsx')
const { v4: uuidv4 } = require("uuid");
const argon2 = require("argon2");
const { User,Student } = require('../../models')
const { formatCapitalizedWord } = require('../../utils')

const file = xlsx.readFile('uploads/users.xlsx')

module.exports = async (req, res) => {
    const sheetName = file.SheetNames[0]

    const defaultPassword =await argon2.hash('123456')

    const sheetData = xlsx.utils.sheet_to_json(file.Sheets[sheetName])

    const result = sheetData.reduce((res, item) => {
        const arrName = item.name.split(' ')
        const email = arrName.shift().replace('.', '') + arrName.pop()

        const userObj = {
            id_user: uuidv4(),
            nama_user: formatCapitalizedWord(item.name),
            email: email.toLowerCase()+Math.floor(Math.random() * 100) + 10 + '@sditalmanarpekanbaru.sch.id',
            password: defaultPassword
        }

        const studentObj = {
            id_siswa: uuidv4(),
            id_user: userObj.id_user,
            nama_siswa: userObj.nama_user,
            nis: item.nis,
            id_kelas: item.kelas
        }

        res['users']=[...res['users'], userObj]
        res['students']=[...res['students'], studentObj]

        return res


    }, {users: [], students: []})

   const users = await User.bulkCreate(result.users)
    const students = await Student.bulkCreate(result.students)


    res.send({users, students})
}