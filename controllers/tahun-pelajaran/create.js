const Validator = require("fastest-validator");
const v = new Validator();

const { TahunPelajaran } = require("../../models");

module.exports = async (req, res) => {
  const schema = {
    nama_tp: "string|empty:false",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length)
    return res.status(400).json({ status: "error", message: validate });

  const data = await TahunPelajaran.create(req.body);

  return res.json({ success: true, data });
};
