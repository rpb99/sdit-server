const { TahunPelajaran } = require("../../models");
const { Op } = require('sequelize')

module.exports = async (req, res) => {
  const data = await TahunPelajaran.findAll({
    where: { nama_tp: { [Op.substring]: req.query.s || "" } },
    limit: 10,
    order: [['created_at', 'DESC']]
  });

  return res.json({ success: true, data });
};
