const { TahunPelajaran } = require("../../models");

module.exports = async (req, res) => {
  const data = await TahunPelajaran.findByPk(req.params.id_tp);

  if (!data) {
    return res
      .status(404)
      .json({ success: false, message: "Data is not found" });
  }

  await data.destroy();

  return res.json({
    success: true,
    message: "Data deleted",
  });
};
