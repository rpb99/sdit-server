const { TahunPelajaran } = require("../../models");

module.exports = async (req, res) => {
    const data = await TahunPelajaran.findOne({ where: { id_tp: req.params.id_tp } });

    return res.json({ success: true, data });
};
