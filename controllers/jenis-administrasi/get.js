const { JenisAdministrasi } = require("../../models");

module.exports = async (req, res) => {
  const {id_jenis_administrasi} = req.params
  const data = await JenisAdministrasi.findOne({ where: { id_jenis_administrasi } });

  if (!data)
    return res
      .status(404)
      .json({ success: false, message: "data is not found" });

  return res.json({ success: true, data });
};
