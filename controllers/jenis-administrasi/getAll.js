const { JenisAdministrasi, TahunPelajaran } = require("../../models");
const {Op} = require('sequelize')

module.exports = async (req, res) => {
  const data = await JenisAdministrasi.findAll({
    include: TahunPelajaran,
    where: { nama_administrasi: { [Op.substring]: req.query.s || "" } },
    limit: 10,
    order: [['created_at', 'DESC']]
  });

  const result = data.reduce((res, item) => {
    const { id_jenis_administrasi, nama_administrasi, keterangan, nominal, TahunPelajaran } = item
    const spp = {
      id_jenis_administrasi,
      nama_administrasi,
      keterangan,
      nominal,
      id_tp: TahunPelajaran.id_tp,
      nama_tp: TahunPelajaran.nama_tp
    }

    return [...res, spp]
  }, [])

  return res.json({ success: true, data:result });
};
