const Validator = require("fastest-validator");
const v = new Validator();
const { v4: uuidv4 } = require("uuid");

const { JenisAdministrasi, Transaksi, Student } = require("../../models");

module.exports = async (req, res) => {
  const schema = {
    nama_administrasi: "string|empty:false",
    nominal: "number|empty:false",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length)
    return res.status(400).json({ status: "error", message: validate });

  const data = await JenisAdministrasi.create(req.body);

  // get all students to fetch an id
  const students = await Student.findAll({ where: { status: 'aktif' } })

  let tempTransactions = []

  for (let i = 0; i < students.length; i++) {
    let tagihanObj = {
      id_transaksi: uuidv4(),
      id_siswa: students[i].id_siswa,
      id_jenis_administrasi: data.id_jenis_administrasi,
      metadata: {
        nama_administrasi: data.nama_administrasi,
        nominal: data.nominal,
        keterangan: data.keterangan
      }
    }

    tempTransactions.push(tagihanObj)
  }

  const transactionResult = await Transaksi.bulkCreate(tempTransactions)

  return res.json({ success: true, data: { jenis_administrasi: data, transactions: transactionResult } });
};
