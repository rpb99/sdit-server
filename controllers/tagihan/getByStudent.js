const { Tagihan, TahunPelajaran, JenisSPP } = require('../../models')

module.exports = async (req, res) => {
    const query = await Tagihan.findAll({
        attributes: ['id_tagihan', 'nama_spp', 'nominal', 'keterangan'],
        include: {
            model: JenisSPP,
            include: {
                model: TahunPelajaran,
                attributes: ['nama_tp']
            }
        },
        where: { id_siswa: req.params.studentId },
        order: [['created_at', 'DESC']]
    })

    const data = query.reduce((result, item) => {
        const bill = {
            id_tagihan: item.id_tagihan,
            nama_spp: item.nama_spp,
            nominal: item.nominal,
            keterangan: item.keterangan,
            tahun_pelajaran: item.JenisSPP.TahunPelajaran.nama_tp
        }

        return [...result, bill]
    }, [])

    res.json({ success: true, data })
}