const create = require("./create");
const getAll = require("./getAll");
const get = require("./get");
const update = require("./update");
const destroy = require("./destroy");
const getByStudent = require("./getByStudent");

module.exports = {
  create,
  getAll,
  get,
  update,
  destroy,
  getByStudent
};
