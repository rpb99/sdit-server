const Validator = require("fastest-validator");
const v = new Validator();

const { Tagihan } = require("../../models");

module.exports = async (req, res) => {
  const schema = {
    id_siswa: "string|empty:false",
    nama_spp: "string|empty:false",
    nominal: "number|empty:false",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length)
    return res.status(400).json({ status: "error", message: validate });

  try {
    const data = await Tagihan.create(req.body);
    return res.json({ success: true, data });

  } catch (error) {
    if (error?.name === 'SequelizeUniqueConstraintError')
      return res.status(409).json({ success: false, message: "Duplicate Entry" });
  }


};
