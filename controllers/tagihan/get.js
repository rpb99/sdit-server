const { JenisSPP } = require("../../models");

module.exports = async (req, res) => {
  const data = await JenisSPP.findByPk(req.params.id_jenis_spp);

  if (!data)
    return res
      .status(404)
      .json({ success: false, message: "data is not found" });

  return res.json({ success: true, data });
};
