const { JenisSPP } = require("../../models");

module.exports = async (req, res) => {
  const data = await JenisSPP.findAll();

  return res.json({ success: true, data });
};
