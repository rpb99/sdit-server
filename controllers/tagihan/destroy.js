const { JenisSPP } = require("../../models");

module.exports = async (req, res) => {
  const data = await JenisSPP.findByPk(req.params.id_jenis_spp);

  if (!data) {
    return res
      .status(404)
      .json({ success: false, message: "Data is not found" });
  }

  await data.destroy();

  return res.json({
    success: true,
    message: "Data deleted",
  });
};
