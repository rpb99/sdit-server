const { Kelas, TahunPelajaran } = require("../../models");
const { Op } = require('sequelize')

module.exports = async (req, res) => {
  const classes = await Kelas.findAll({
    include: TahunPelajaran,
    where: { nama_kelas: { [Op.substring]: req.query.s || "" } },
    order: [['nama_kelas', 'ASC']]
  });

  const data = classes.reduce((result, item) => {
    const classItem = {
      id_kelas: item.id_kelas,
      id_tp: item.id_tp,
      nama_kelas: item.nama_kelas,
      nama_tp: item.TahunPelajaran.nama_tp
    }

    return [...result, classItem]

  }, [])

  return res.json({ success: true, data });
};
