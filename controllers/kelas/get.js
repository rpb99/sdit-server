const { Kelas } = require("../../models");

module.exports = async (req, res) => {
    const data = await Kelas.findOne({ where: { id_kelas: req.params.id_kelas } });

    return res.json({ success: true, data });
};
