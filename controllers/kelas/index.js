const create = require("./create");
const getAll = require("./getAll");
const getAllSelect = require("./getAllSelect");
const update = require("./update");
const destroy = require("./destroy");
const get = require("./get");

module.exports = {
  create,
  getAll,
  get,
  update,
  destroy,
  getAllSelect
};
