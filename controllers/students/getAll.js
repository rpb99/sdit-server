const { Student, Kelas, User } = require("../../models");
const { Op } = require("sequelize");

module.exports = async (req, res) => {
  const { nis, nama } = req.query;

  const students = await Student.findAll({
    attributes: ['id_siswa', 'nis', 'nama_siswa'],
    include: [
      { model: Kelas, attributes: ['id_kelas', 'nama_kelas'] },
      {
        model: User, attributes: ['id_user', 'nama_user', 'email']
      }
    ],
    where: {
      [Op.or]: [
        { nama_siswa: { [Op.substring]: nama || "" } },
        { nis: { [Op.substring]: nis || "" } },
      ],
    },
  });

  const data = students.reduce((result, item) => {
    const { id_siswa, telepon, nis, nama_siswa, jenis_kelamin, alamat, User, Kela } = item
    const jenisKelamin = jenis_kelamin === 'L' ? 'Laki-laki' : 'Perempuan'
    const student = {
      id_siswa,
      alamat,
      jenis_kelamin: jenisKelamin,
      id_kelas: Kela.id_kelas,
      nama_kelas: Kela.nama_kelas,
      nama_siswa,
      id_user: User.id_user,
      nama_user: User.nama_user,
      email: User.email,
      nis,
      telepon,
    }


    return [...result, student]
  }, [])
  return res.json({ success: true, data });
};
