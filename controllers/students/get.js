const { Student } = require("../../models");

module.exports = async (req, res) => {
  const student = await Student.findOne({where: {id_user: req.params.userId}});


  if (!student)
    return res
      .status(404)
      .json({ success: false, message: "Student is not found" });

  return res.json({ success: true, data: student });
};
