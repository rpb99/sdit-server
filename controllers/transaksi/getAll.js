const { QueryTypes } = require('sequelize');
const db = require('../../models');
module.exports = async (req, res) => {
    let search = req.query.s || ""
    let adm = req.query.adm || ""
    let kelas = req.query.kelas || ""
    let orderBy = req.query.orderBy || 'DESC'

    const queryNoFilters = await db.sequelize.query(
        `
    SELECT 
        ROW_NUMBER() OVER () no,
        adm.nama_administrasi, 
        siswa.nama_siswa, 
        siswa.nis, 
        kelas.nama_kelas,
        tp.nama_tp,
        transaksi.id_transaksi,
        transaksi.tgl_bayar,
        JSON_EXTRACT(transaksi.metadata, '$.nama_administrasi') nama_administrasi,
        JSON_EXTRACT(transaksi.metadata, '$.nominal') nominal 
    FROM transaksi
    JOIN jenis_administrasi adm 
        ON adm.id_jenis_administrasi = transaksi.id_jenis_administrasi
    JOIN siswa 
        ON siswa.id_siswa = transaksi.id_siswa
    JOIN kelas
        ON kelas.id_kelas = siswa.id_kelas
    JOIN tahun_pelajaran tp
        ON tp.id_tp = adm.id_tp
    WHERE 
        (
        LOWER(JSON_EXTRACT(transaksi.metadata,"$.nama_administrasi")) LIKE LOWER('%${search}%')
            OR
        LOWER(siswa.nama_siswa) LIKE '%${search}%'
            OR
        LOWER(siswa.nis) LIKE '%${search}%'
        )
            AND
        transaksi.status = 'success'
    ORDER BY transaksi.tgl_bayar DESC
    LIMIT 10
    `
        , { type: QueryTypes.SELECT });

    const queryFilters = await db.sequelize.query(
        `
    SELECT 
        ROW_NUMBER() OVER () no,
        adm.nama_administrasi, 
        siswa.nama_siswa, 
        siswa.nis, 
        kelas.nama_kelas,
        tp.nama_tp,
        transaksi.id_transaksi,
        transaksi.tgl_bayar,
        JSON_EXTRACT(transaksi.metadata, '$.nama_administrasi') nama_administrasi,
        JSON_EXTRACT(transaksi.metadata, '$.nominal') nominal 
    FROM transaksi
    JOIN jenis_administrasi adm 
        ON adm.id_jenis_administrasi = transaksi.id_jenis_administrasi
    JOIN siswa 
        ON siswa.id_siswa = transaksi.id_siswa
    JOIN kelas
        ON kelas.id_kelas = siswa.id_kelas
    JOIN tahun_pelajaran tp
        ON tp.id_tp = adm.id_tp
    WHERE 
        (
        LOWER(JSON_EXTRACT(transaksi.metadata,"$.nama_administrasi")) LIKE LOWER('%${search}%')
            OR
        LOWER(siswa.nama_siswa) LIKE '%${search}%'
            OR
        LOWER(siswa.nis) LIKE '%${search}%'
        )
            AND
        transaksi.id_jenis_administrasi = '${adm}'
            AND
        transaksi.status = 'success'
    ORDER BY transaksi.tgl_bayar ${orderBy}
    `
        , { type: QueryTypes.SELECT });

    const priceFilters = queryFilters.reduce((acc, curr) => {
        return curr.nominal + acc
    }, 0)
    const priceNoFilters = queryNoFilters.reduce((acc, curr) => {
        return curr.nominal + acc
    }, 0)


    const data = !!adm ? queryFilters : queryNoFilters
    const price = !!adm ? priceFilters : priceNoFilters

    res.json({ success: true, data, total_price:price })
}