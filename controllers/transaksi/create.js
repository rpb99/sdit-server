const midtransClient = require('midtrans-client');
const { Tagihan, Student, JenisSPP, User, Transaksi } = require('../../models')
const { randomStr } = require('../../utils')

module.exports = async (req, res) => {

    const transaction = await Transaksi.findOne({
        include: {
            model: Student,
            attributes: ['nama_siswa'],
            include: {
                model: User,
                attributes: ['email']
            }
        },
        where: {
            id_transaksi: req.body.id_transaksi
        }
    })

    const expired = new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate() + " " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds() + " +0700"


    const itemDetails = { id: transaction.id_transaksi, price: transaction.metadata.nominal, quantity: 1, name: transaction.metadata.nama_administrasi }

    // Create Snap API instance
    let snap = new midtransClient.Snap({
        // Set to true if you want Production Environment (accept real transaction).
        isProduction: false,
        serverKey: process.env.SERVER_KEY
    });

    let parameter = {
        "transaction_details": {
            "order_id": itemDetails.id + '_' + randomStr(5),
            "gross_amount": itemDetails.price
        },
        'item_details': itemDetails,
        "customer_details": {
            "first_name": transaction.Student.nama_siswa,
            "email": transaction.Student.User.email,
        },
        "expiry": {
            "start_time": expired,
            "unit": "hours",
            "duration": 6
        }
    };

    const transactionMidtrans = await snap.createTransaction(parameter)

    let snapUrl = transactionMidtrans.redirect_url;

    transaction.snap_url = snapUrl

    transaction.save()

    return res.json({ success: true, data: transaction });
}
