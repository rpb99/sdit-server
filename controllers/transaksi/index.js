const create = require('./create')
const getAllBills = require('./getAllBills')
const getAllSuccessByStudent = require('./getAllSuccessByStudent')
const get = require('./get')
const getAll = require('./getAll')
const webhook = require('./webhook')

module.exports = {
    create,
    getAllBills,
    getAllSuccessByStudent,
    get,
    webhook,
    getAll
}