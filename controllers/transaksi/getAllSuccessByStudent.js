const { Transaksi, JenisAdministrasi, TahunPelajaran } = require('../../models')
const { Op } = require('sequelize')

module.exports = async (req, res) => {
    const query = await Transaksi.findAll({
        include: { model: JenisAdministrasi, include: TahunPelajaran, where:{nama_administrasi: { [Op.substring]: req.query.s || "" } }},
        where: { id_siswa: req.params.studentId, status: 'success' }
    })

    const data = query.reduce((result, item) => {
        const transaction = {
            id_transaksi: item.id_transaksi,
            nama_administrasi: item.metadata.nama_administrasi,
            nominal: item.metadata.nominal,
            keterangan: item.metadata.keterangan,
            tahun_ajaran: item.JenisAdministrasi.TahunPelajaran.nama_tp,
            tgl_bayar: item.tgl_bayar,
            status: item.status
        }

        return [...result, transaction]
    }, [])

    res.json({ success: true, data })
}