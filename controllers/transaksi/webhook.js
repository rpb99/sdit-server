const crypto = require('crypto')
const { Transaksi, PaymentLog } = require('../../models')

module.exports = async (req, res) => {
    const hash = crypto.createHash('sha512')


    const signatureKey = req.body.signature_key
    const orderId = req.body.order_id
    const statusCode = req.body.status_code
    const grossAmount = req.body.gross_amount
    const serverKey = process.env.SERVER_KEY

    const data = orderId + statusCode + grossAmount + serverKey
    const hashData = hash.update(data, 'utf-8')
    const mySignatureKey = hashData.digest('hex');

    const transactionStatus = req.body.transaction_status
    const type = req.body.payment_type
    const fraudStatus = req.body.fraud_status

    if (signatureKey !== mySignatureKey) {
        return res.status(400).json({ success: false, message: 'invalid signature' })
    }
    const idTransaksi = orderId.split('_')[0]
    const transaksi = await Transaksi.findOne({ where: { id_transaksi: idTransaksi } })

    if (!transaksi) {
        return res.status(404).json({ success: false, message: 'transaksi is not found' })
    }

    if (transaksi.status === 'success') {
        return res.status(405).json({ success: false, message: 'operation not permitted' })
    }



    if (transactionStatus == 'capture') {
        if (fraudStatus == 'challenge') {
            transaksi.status = 'challenge'
        } else if (fraudStatus == 'accept') {
            transaksi.tgl_bayar = new Date().toString()
            transaksi.status = 'success'
        }
    } else if (transactionStatus == 'settlement') {
        transaksi.tgl_bayar = new Date().toString()
        transaksi.status = 'success'
    } else if (transactionStatus == 'cancel' ||
        transactionStatus == 'deny' ||
        transactionStatus == 'expire') {
        transaksi.status = 'failure'
    } else if (transactionStatus == 'pending') {
        transaksi.status = 'pending'
    }

    const logData = {
        status: transactionStatus,
        raw_response: req.body,
        id_transaksi: idTransaksi,
        payment_type: type
    }

    await PaymentLog.create(logData)
    await transaksi.save()

    res.send('ok')
    // return true
}