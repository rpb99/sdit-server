const { Transaksi, JenisSPP, TahunPelajaran } = require('../../models')
const { Op, QueryTypes } = require('sequelize')
const db = require('../../models')

module.exports = async (req, res) => {
    let search = req.query.s || ""

    const data = await db.sequelize.query(`
    SELECT 
        trx.id_transaksi,
        trx.updated_at,
        JSON_EXTRACT(trx.metadata, '$.nama_administrasi') nama_administrasi,
        JSON_EXTRACT(trx.metadata, '$.nominal') nominal,
        JSON_EXTRACT(trx.metadata, '$.keterangan') keterangan,
        tp.nama_tp
    FROM transaksi trx
    JOIN jenis_administrasi adm
        ON adm.id_jenis_administrasi = trx.id_jenis_administrasi
    JOIN tahun_pelajaran tp
        ON tp.id_tp = adm.id_tp
    WHERE 
        trx.status = 'pending'
            AND
        trx.id_siswa = '${req.params.studentId}'
            AND
        LOWER(JSON_EXTRACT(trx.metadata,"$.nama_administrasi")) LIKE LOWER('%${search}%')
    ORDER BY trx.created_at DESC
    `, { type: QueryTypes.SELECT })

    if (!data.length) return res.status(404).json({ success: false, message: "Data not found!" })


    // const data = query.reduce((result, item) => {
    //     const transaction = {
    //         id_transaksi: item.id_transaksi,
    //         nama_administrasi: item.metadata.nama_administrasi,
    //         nominal: item.metadata.nominal,
    //         keterangan: item.metadata.keterangan,
    //         tahun_ajaran: item.JenisSPP.TahunPelajaran.nama_tp,
    //         tgl_bayar: item.tgl_bayar,
    //         status: item.status,
    //         updatedAt: item.updatedAt
    //     }

    //     return [...result, transaction]
    // }, [])

    return res.json({ success: true, data })
}