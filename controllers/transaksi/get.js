const { Transaksi } = require('../../models')

module.exports = async (req, res) => {
    const query = await Transaksi.findOne({ where: { id_transaksi: req.params.transactionId } })

    if (!query) return res.status(404).json({success:false, message: 'Transaction is not found'})

    res.json({ success: true, data: query })
}