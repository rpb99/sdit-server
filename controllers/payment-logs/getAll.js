const { PaymentLog, Transaksi, Student, Kelas, JenisSPP, TahunPelajaran } = require('../../models')
const { Op } = require('sequelize')
module.exports = async (req, res) => {
    const query = await PaymentLog.findAll({
        include: {
            model: Transaksi,
            // where: { metadata: { nama_spp: { [Op.substring]: req.query.s || "" } } },
            include: [
                {
                    model: Student,
                    where: {
                        nama_siswa: { [Op.substring]: req.query.s || "" }
                    },
                    include: Kelas,
                },
                { model: JenisSPP, include: TahunPelajaran }
            ]
        },
        attributes: ['raw_response', 'id_transaksi'],
    })

    res.send(query)

    // const data = query.reduce((result, item, idx) => {
    //     if (!item.Transaksi) return res.status(404).json({ success: false, message: 'Data not found!' })
    //     const log = {
    //         id_payment_logs: item.id_payment_logs,
    //         no_transaksi: item.id_transaksi,
    //         nama_spp: item.Transaksi.metadata.nama_spp,
    //         nominal: item.Transaksi.metadata.nominal,
    //         keterangan: item.Transaksi.metadata.keterangan,
    //         tahun_ajaran: item.Transaksi.JenisSPP.TahunPelajaran.nama_tp,
    //         tgl_bayar: item.Transaksi.tgl_bayar,
    //         status: item.Transaksi.status,
    //         nis: item.Transaksi.Student.nis,
    //         nama_siswa: item.Transaksi.Student.nama_siswa,
    //         kelas: item.Transaksi.Student.Kela.nama_kelas,
    //         numbering: idx + 1
    //     }

    //     return [...result, log]
    // }, [])



    // return res.json({ success: true, data })
}