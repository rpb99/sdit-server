const login = require("./login");
const register = require("./register");
const logout = require("./logout");
const currentUser = require("./currentUser");
const updateProfile = require("./updateProfile");
const getAll = require("./getAll");
const getAllAvailable = require("./getAllAvailable");
const destroy = require("./destroy");

module.exports = { getAll, login, register, logout, currentUser, updateProfile, destroy,getAllAvailable };
