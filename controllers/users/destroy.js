const {User} = require('../../models')

module.exports = async (req,res) => {
    const data = await User.findByPk(req.params.id_user);

    if (!data) {
      return res
        .status(404)
        .json({ success: false, message: "Data is not found" });
    }
  
    await data.destroy();
  
    return res.json({
      success: true,
      message: "Data deleted",
    });
}