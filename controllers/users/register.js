const Validator = require("fastest-validator");
const v = new Validator();

const { User } = require("../../models");
const { sendTokenRes } = require("../../helper");
const { formatCapitalizedWord } = require("../../utils");

module.exports = async (req, res) => {
  const schema = {
    nama_user: "string|empty:false",
    email: "string|empty:false",
    // password: "string|min:6",
    avatar: "string|optional",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length)
    return res.status(400).json({ status: "error", message: validate });

  try {
    // if (req.body.id_siswa) {
    //   const student = await Student.findOne({
    //     where: { id: req.body.id_siswa },
    //   });

    //   if (!student) {
    //     return res
    //       .status(404)
    //       .json({ status: "error", message: "Siswa belum terdaftar" });
    //   }
    // }

req.body.nama_user = formatCapitalizedWord(req.body.nama_user)

    const user = await User.create(req.body);

    const data = {
      nama_user: user.nama_user,
      email: user.email
    }
    res.json({ success: true, data })
    // sendTokenRes(user, 201, res);
  } catch (error) {
    if (error.errors?.[0]?.message.includes("UNIQUE_USER_EMAIL"))
      return res
        .status(400)
        .json({ success: false, message: "Email must be unique" });

    return res.status(400).json({ success: false, message: "Error" });
  }
};
