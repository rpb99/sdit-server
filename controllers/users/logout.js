const { RefreshToken } = require("../../models");

module.exports = async (req, res) => {
  // const refreshToken = req.cookies.refreshToken;

  // await RefreshToken.destroy({ where: { token: refreshToken } });

  // res.cookie("refreshToken", "none", {
  //   expires: new Date(new Date().getTime() + 1),
  //   httpOnly: true,
  // });

  // res.clearCookie("refreshToken", { path: "/" });
  // return res.json({ success: true, data: {} });

  const refreshToken = req.cookies.refreshToken;

  if (!refreshToken) return res.sendStatus(403);

  const token = await RefreshToken.findOne({
    where: {
      token: refreshToken,
    },
  });

  if (!token) return res.sendStatus(204);

  await token.destroy();

  res.clearCookie("refreshToken");
  return res.sendStatus(200);
};
