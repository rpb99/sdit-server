const { Student } = require('../../models')
const db = require('../../models');
const { Op, QueryTypes } = require('sequelize')

module.exports = async (req, res) => {
    const { nama = "" } = req.query

    // Check user exist in student
    let userIds = [0]
    const students = await Student.findAll({ attributes: ['id_user'] })
    students.map(student => userIds.push(student.id_user))

    const users = await db.sequelize.query(`
    SELECT * FROM users
    WHERE 
        role = 'student'
            AND
        id_user NOT IN(SELECT id_user FROM siswa)
    `
    , { type: QueryTypes.SELECT });

    // const users = await User.findAll({
    //     attributes: ['id_user', 'nama_user', 'email'],
    //     where: {
    //         // [Op.not]: [
    //         //     {id_user: userIds}
    //         // ],            
    //         role: 'student',
    //         // nama_user: Sequelize.where(Sequelize.fn('LOWER', Sequelize.col('nama_user')), 'LIKE', '%' + nama.toLowerCase() + '%')
    //     },
    //     limit: 10
    // })

    return res.json({ success: true, data: users })
}