const { User } = require("../../models");

module.exports = async (req, res) => {
  try {
    const user = await User.findOne({
      attributes: ["id_user", "nama_user", "email", "role"],
      where: { id_user: req.user?.id_user },
    });

    return res.json({ success: true, data: user });
  } catch (error) {
    console.log(error)
  }
};
