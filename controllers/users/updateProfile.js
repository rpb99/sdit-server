const Validator = require("fastest-validator");
const v = new Validator();

const { User } = require("../../models");
const { formatCapitalizedWord } = require("../../utils");

module.exports = async (req, res) => {
  const schema = {
    nama_user: "string|empty:false",
    email: "string|empty:false",
  };

  const validate = v.validate(req.body, schema);

  if (validate.length)
    return res.status(400).json({ status: "error", message: validate });

  const { nama_user, email} = req.body;

  const data = { nama_user:formatCapitalizedWord(nama_user), email };

  const user = await User.findOne({ where: { email } });

  if (!user)
    return res
      .status(404)
      .json({ success: false, message: "user is not found" });

  // if (user.email !== req.user.email)
  //   return res
  //     .status(401)
  //     .json({ success: false, message: "method not allowed" });

  await user.update(data);

  return res.json({ success: true, data });
};
