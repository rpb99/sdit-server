const { User, Student } = require('../../models')
const { Op, Sequelize,QueryTypes } = require('sequelize')
const db = require('../../models')

module.exports = async (req, res) => {
    const nama = req.query.nama || ""

    const users = await db.sequelize.query(`
        SELECT 
            id_user, 
            nama_user,
            email
        FROM users
        WHERE
            role = 'student'
                AND
            LOWER(nama_user) LIKE LOWER('%${nama}%')
        ORDER BY nama_user
        LIMIT 10
    `, {type: QueryTypes.SELECT})

    return res.json({ success: true, data: users })
}